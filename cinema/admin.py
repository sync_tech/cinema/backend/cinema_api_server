from django.contrib import admin
from .models import Movie, Cinema, CinemaBranch, CinemaRoom
# Register your models here.


admin.site.register(Movie)
admin.site.register(Cinema)
admin.site.register(CinemaBranch)
admin.site.register(CinemaRoom)