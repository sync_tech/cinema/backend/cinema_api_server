from django.db import models
import uuid
# Create your models here.
'''
Movie
Cinema
Cinema_branch
Cinema_room

'''
'''
Cinema_subscription
schedule managment
'''
'''
Image_Container
# Rating
# Notification
#Commenting
'''


# def images_path():
#     return os.path.join(settings.LOCAL_FILE_DIR, 'images')


class Movie(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=50)
    genre = models.CharField(max_length=30,null=True, blank=True)
    # an array of images or image name
    poster = models.ImageField(upload_to="poster", null=True, blank=True)
    time_lapse = models.CharField(max_length=20, null=True, blank=True)
    writer = models.CharField(max_length=50,null=True, blank=True)
    director = models.CharField(max_length=50,null=True, blank=True)
    producer = models.CharField(max_length=50,null=True, blank=True)
    # rating will be update soon
    rating = models.DecimalField(max_digits=4, decimal_places=2, default=2)
    # cast an array of character
    cast = models.CharField(max_length=50,null=True, blank=True)
    # category will be either a model or enum
    category = models.CharField(max_length=50,null=True, blank=True)
    # status will be either a model or enum
    status = models.CharField(max_length=50,null=True, blank=True)
    language = models.CharField(max_length=30,null=True, blank=True)
    trailer = models.CharField(max_length=200,null=True, blank=True)
    release_date = models.DateField(auto_now=True)
    view_count = models.DecimalField(max_digits=20, decimal_places=2,default=2)
    description = models.TextField(default='Description will be here...')
    # refer to user
    posted_by = models.CharField(max_length=10, default="Admin")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Cinema(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4(), editable=False)
    name = models.CharField(max_length=30)
    logo = models.ImageField(upload_to="cinema_logo", null=True, blank=True)
    photos = models.CharField(max_length=10, null=True, blank=True)
    # refer to user
    admin = models.CharField(max_length=20, default="Admin")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class CinemaBranch(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=30)
    lat = models.FloatField('Latitude', blank=True, null=True)
    lon = models.FloatField('Longitude', blank=True, null=True)
    email = models.CharField(max_length=50, null=True, blank=True)
    phone_num = models.CharField(max_length=30, null=True, blank=True)
    address = models.CharField(max_length=300, null=True, blank=True)
    # refer to user
    admin = models.CharField(max_length=20, default="Admin")
    # will be an array of gallery
    gallery = models.ImageField(upload_to="gallery", null=True, blank=True)
    cinema = models.ForeignKey(Cinema, related_name='cinema', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class CinemaRoom(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=20,blank=True, null=True)
    total_seat = models.DecimalField(max_digits=20, decimal_places=2)
    #refer to admin user
    admin = models.CharField(max_length=20, default="Admin")
    # will be an array of gallery
    gallery = models.ImageField(upload_to="gallery", null=True, blank=True)
    # direction = models.CharField(max_length=30)
    branch = models.ForeignKey(CinemaBranch, related_name='branch', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
