from rest_framework import serializers
from .models import Movie, Cinema, CinemaBranch, CinemaRoom


class MovieSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie
        fields = '__all__'


class CinemaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cinema
        fields = '__all__'


class CinemaBranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = CinemaBranch
        fields = '__all__'


class CinemaRoomSerializer(serializers.ModelSerializer):

    class Meta:
        model = CinemaRoom
        fields = '__all__'