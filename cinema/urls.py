from django.urls import path, include
from rest_framework import routers
# from rest_framework.authtoken.views import obtain_auth_token
from .views import MovieViewSet, CinemaViewSet, CinemaBranchViewSet, CinemaRoomViewSet

router = routers.DefaultRouter()

router.register('movie', MovieViewSet)
router.register('cinema', CinemaViewSet)
router.register('cinema_branch', CinemaBranchViewSet )
router.register('cinema_hall', CinemaRoomViewSet )


urlpatterns = [
    path('', include(router.urls)),
    # path('token/', obtain_auth_token, name="obtain_token_auth"),

    # path('user/', AuthorizedUserViewSet.as_view()),
    # path('manage-users/', .as_view()),
    # path('salesproduct/<uuid:pk>', SaleDetailView.as_view()),

]
