from django.shortcuts import render
from rest_framework import viewsets
from .serializers import MovieSerializer, CinemaSerializer, CinemaBranchSerializer, CinemaRoomSerializer
from .models import Movie, Cinema, CinemaBranch, CinemaRoom


# Create your views here.


class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class CinemaViewSet(viewsets.ModelViewSet):
    queryset = Cinema.objects.all()
    serializer_class = CinemaSerializer


class CinemaBranchViewSet(viewsets.ModelViewSet):
    queryset = CinemaBranch.objects.all()
    serializer_class = CinemaBranchSerializer


class CinemaRoomViewSet(viewsets.ModelViewSet):
    queryset = CinemaRoom.objects.all()
    serializer_class = CinemaRoomSerializer
